from flask import Flask, render_template, url_for, request, session, redirect
from pymongo import MongoClient
from bson.objectid import ObjectId

app = Flask(__name__)
app.secret_key = 'super secret key'

# config mongodb local
client = MongoClient("mongodb://popo:test123@cluster0-shard-00-00-yux0y.mongodb.net:27017/primer?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin")
db = client.user
# collection == table --> documents inside
collection = db['user']

@app.route('/')
def index():
    list = []
    nickname = []

    for document in collection.find():
        list.append(document)

    if 'nickname' in session:
        nickname.append(session['nickname'])

    return render_template('home.html', list=list, nickname=nickname)

@app.route('/login', methods=['POST'])
def login():
    login_user = collection.find_one({'nickname' : request.form['nickname']})

    if login_user:
        session['nickname'] = request.form['nickname']
        return redirect(url_for('index'))

    return 'Nickname not found'

@app.route('/add', methods=['POST'])
def add():
    name = request.form['nickname']
    collection.insert({"nickname": name})
    return render_template('end.html', message='nickname %s added' % name)

@app.route('/create')
def create():
    return render_template('create.html')

@app.route('/update/<nickname>', methods=['POST'])
def update(nickname):
    try:
        new_nickname = request.form['nickname']
        collection.update_one({'nickname': nickname}, {'$set': {'nickname': new_nickname}}, upsert=True )
        return render_template('end.html', message='nickname %s updated to %s' % (nickname, new_nickname))
    except Exception as e:
        return render_template('end.html', message='Error occured %s' % e)

@app.route('/update/<id>')
def update_id(id):
    name = collection.find_one({"_id": ObjectId(str(id))})['nickname']
    return render_template('create.html', nickname=name)

@app.route('/delete/<id>', methods=['GET'])
def delete(id):
    try:
        name = collection.find_one({"_id": ObjectId(str(id))})['nickname']
        collection.delete_one({"_id": ObjectId(str(id))})
        return render_template('end.html', message='nickname %s deleted' % name)
    except Exception as e:
        return render_template('end.html', message='Error occured %s' % e)

@app.route('/drop')
def drop_collection():
    collection.drop()
    return render_template('end.html', message='collection dropped')


if __name__ == "__main__":

    app.config['SESSION_TYPE'] = 'mongodb'

    sess.init_app(app)

    app.debug = True
    app.run()

